<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">

    
<!-- Mirrored from azmind.com/demo/bootstrap-login-register-forms/form-2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Apr 2016 09:28:14 GMT -->
<head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        
        <link href=" <c:url value="/resources/bootstrap/css/bootstrap.min.css"/> " rel="stylesheet" />
        
        <link href=" <c:url value="/resources/font-awesome/css/font-awesome.min.css"/> " rel="stylesheet" />
        
		<link href=" <c:url value="/resources/css/form-elements.css"/> " rel="stylesheet" />
		
        <link href=" <c:url value="/resources/css/style.css"/> " rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link href=" <c:url value="/resources/ico/favicon.png"/> " rel="stylesheet" />
        <link href=" <c:url value="/resources/ico/apple-touch-icon-144-precomposed.png"/> " rel="stylesheet" />
        <link href=" <c:url value="/resources/ico/apple-touch-icon-114-precomposed.png"/> " rel="stylesheet" />
        <link href=" <c:url value="/resources/ico/apple-touch-icon-72-precomposed.png"/> " rel="stylesheet" />
        <link href=" <c:url value="/resources/ico/apple-touch-icon-57-precomposed.png"/> " rel="stylesheet" />

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                <c:url value="/logout" var="logoutUrl" />
									<form action="${logoutUrl}" method="post" id="logoutForm">
										<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                    	<li><button type="submit" class="btn btn-default">Log out</button></li>  
                                    </form>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Result Point</strong></h1>
                            <div class="description">
                            	<p>
	                            	This is exams result managing API.
                            	</p>
                            	<p>
	                            	Fast & Easy access. Open API for use anywhere, Secure API with more functionalities
                            	</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                        	
                        	<div class="form-box">
	                        	<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>University Details</h3>
	                            		<p>Check your results repository</p>
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="fa fa-university"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
				                    <form role="form" action="#" method="post" class="login-form">
				                    	<div class="form-group">
				                    		<label class="" for="form-username">University Name &nbsp;&nbsp;&nbsp;:</label>
                                            <spane style="color:#fff;" class="max" for="form-username">${uni.name}</spane>
				                        	
				                        </div>
				                        <div class="form-group">
				                        	<label class="" for="form-password">Release Results &nbsp;&nbsp;&nbsp;&nbsp;:</label>
				                        	<spane style="color:#fff;" class="max" for="form-username"><strong>${count}</strong></spane>
				                        </div>
                                        <div class="form-group">
				                        	<label class="" for="form-password">Results View &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
				                        	<spane style="color:#fff;" class="max" for="form-username"><strong>${uni.viewCount}</strong></spane>
				                        </div>
                                        <div class="form-group">
				                        	<label class="" for="form-password">Register Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
				                        	<spane style="color:#fff;" class="max" for="form-username">
				                        	${uni.registerDate}
				                        	</spane>
				                        </div>
				                    </form>
			                    </div>
		                    </div>          
                        </div>
                        
                        	
                        <div class="col-sm-8">
                        	<div class="form-box">
                        		<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>Token</h3>
	                            		<p>Use this token for authenticate</p>
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="fa fa-key"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
				                    <form role="form" action="<c:url value="/dashboard"/>" method="post" class="registration-form">
				                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				                        <c:choose>
				                        	<c:when test="${uni.token != null }">
				                        		<h3 style="color:#fff; text-align:center;">${uni.token}</h3>
				                        	</c:when>
				                        	<c:otherwise>
				                        		<h3 style="color:#fff; text-align:center;">Generate your authentication token</h3>
				                        		<button type="submit" class="btn">Generate</button>
				                        	</c:otherwise>
				                        </c:choose>
				                    </form>
			                    </div>
                        	</div>
                        </div>

                        <div class="col-sm-12">
                            
                            <div class="form-box">
                                <div class="form-top">
                                    <div class="form-top-left">
                                        <h3>Secure API Documentation</h3>
                                        <p>Refer Documentation to develop your application with result point secure API</p>
                                    </div>
                                    <div class="form-top-right">
                                        <i class="fa fa-file-text"></i>
                                    </div>
                                </div>
                                <div class="col-md-12 bgColor">
									<p class="col-sm-6 bg">
                                       Response Objects - <br><br>
                                       *&nbsp;Success Response <br>
                                       &nbsp;{<br>
                                       &nbsp;&nbsp;&nbsp;&nbsp;indexNo : "{index number of result}",<br>
                                       &nbsp;&nbsp;&nbsp;&nbsp;type : "{type of result}",<br>
                                       &nbsp;&nbsp;&nbsp;&nbsp;rClass : "{result class}",<br>
                                       &nbsp;&nbsp;&nbsp;&nbsp;marks : "{marks}",<br>
                                       &nbsp;}<br>
                                       *&nbsp;Error Response <br>
                                       &nbsp;{<br>
                                       &nbsp;&nbsp;&nbsp;&nbsp;eMsg : "{Error Message}",<br>
                                       &nbsp;&nbsp;&nbsp;&nbsp;statusCode : "{http status code}",<br>
                                       &nbsp;}<br><br>
                                       -----------------------------------------------------------------------------------------------------
                                    </p>
                                    <p class="col-sm-6 bg">
                                       <br>Get All Results - <br><br>
                                       *&nbsp;Request Method - GET <br>
                                       *&nbsp;Request URL - www.resultpoint.com/api/secure/{token} <br>
                                       *&nbsp;Response Type - JSON<br>
                                       *&nbsp;Response Contain - List of Result Objects<br>
                                       -----------------------------------------------------------------------------------------------------
                                    </p>  
                                    <p class="col-sm-6 bg">
                                       <br>Get Result - <br><br>
                                       *&nbsp;Request Method - GET <br>
                                       *&nbsp;Request URL - www.resultpoint.com/api/secure/{indexNo}/{token} <br>
                                       *&nbsp;Response Type - JSON<br>
                                       *&nbsp;Response Contain - Result Objects<br>
                                       -----------------------------------------------------------------------------------------------------
                                    </p>   
                                    <p class="col-sm-6 bg">
                                        <br>Publish Result - <br><br>
                                       *&nbsp;Request Method - POST <br>
                                       *&nbsp;Request Object - Result Objects<br>
                                       *&nbsp;Request URL - www.resultpoint.com/api/secure/{token} <br>
                                       *&nbsp;Response Type - JSON<br>
                                       *&nbsp;Response Contain - Result Objects<br>
                                       -----------------------------------------------------------------------------------------------------
                                    </p>  
                                    <p class="col-sm-6 bg">
                                       <br>Update Results - <br><br>
                                       *&nbsp;Request Method - PUT <br>
                                       *&nbsp;Request Object - Result Objects<br>
                                       *&nbsp;Request URL - www.resultpoint.com/api/secure/{indexNo}/{token} <br>
                                       *&nbsp;Response Type - JSON<br>
                                       *&nbsp;Response Contain - Result Objects<br>
                                       -----------------------------------------------------------------------------------------------------
                                    </p>  
									<p class="col-sm-6 bg">
                                       <br>Delete Results - <br><br>
                                       *&nbsp;Request Method - DELETE <br>
                                       *&nbsp;Request URL - www.resultpoint.com/api/secure/{indexNo}/{token} <br>
                                       *&nbsp;Response Type - JSON<br>
                                       *&nbsp;Response Contain - Result Objects<br>
                                       -----------------------------------------------------------------------------------------------------
                                    </p>
                                </div>
                                
                        
                            </div>          
                        </div>    



                    </div>
                    
                </div>
            </div>
            
        </div>

        <!-- Footer -->
        <footer>
        	<div class="container">
        		<div class="row">
        			
        			<div class="col-sm-8 col-sm-offset-2">
        				<div class="footer-border"></div>
        			</div>
        			
        		</div>
        	</div>
        </footer>

        <!-- Javascript -->
        <script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/jquery.backstretch.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/scripts.js"/>"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>
</html>