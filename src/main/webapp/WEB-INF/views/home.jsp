<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">

    
<!-- Mirrored from azmind.com/demo/bootstrap-login-register-forms/form-2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Apr 2016 09:28:14 GMT -->
<head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        
        <link href=" <c:url value="/resources/bootstrap/css/bootstrap.min.css"/> " rel="stylesheet" />
        
        <link href=" <c:url value="/resources/font-awesome/css/font-awesome.min.css"/> " rel="stylesheet" />
        
		<link href=" <c:url value="/resources/css/form-elements.css"/> " rel="stylesheet" />
		
        <link href=" <c:url value="/resources/css/style.css"/> " rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link href=" <c:url value="/resources/ico/favicon.png"/> " rel="stylesheet" />
        <link href=" <c:url value="/resources/ico/apple-touch-icon-144-precomposed.png"/> " rel="stylesheet" />
        <link href=" <c:url value="/resources/ico/apple-touch-icon-114-precomposed.png"/> " rel="stylesheet" />
        <link href=" <c:url value="/resources/ico/apple-touch-icon-72-precomposed.png"/> " rel="stylesheet" />
        <link href=" <c:url value="/resources/ico/apple-touch-icon-57-precomposed.png"/> " rel="stylesheet" />

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                	
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Result Point</strong></h1>
                            <div class="description">
                            	<p>
	                            	This is exams result managing API.
                            	</p>
                            	<p>
	                            	Fast & Easy access. Open API for use anywhere, Secure API with more functionalities
                            	</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-5">
                        	
                        	<div class="form-box">
	                        	<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>Login</h3>
	                            		<p>Login to manage your secure API credentials</p>
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="fa fa-lock"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
	                            <c:url var="loginUrl" value="/login" />
				                    <form role="form" action="${loginUrl}" method="post" class="login-form">
				                    	<c:if test="${not empty error}">
				                    		<div class="alert alert-danger">
			                                    <p>${error}</p>
			                                </div>
										</c:if>
										<c:if test="${not empty msg}">
											<div class="alert alert-success">
			                                    <p>${msg}</p>
			                                </div>
										</c:if>
				                    	<div class="form-group">
				                    		<label class="sr-only" for="form-username">University</label>
				                        	<input type="text" name="username" placeholder="University..." class="form-username form-control" id="form-username">
				                        	
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-password">Password</label>
				                        	<input type="password" name="password" placeholder="Password..." class="form-password form-control" id="form-password">
				                        </div>
				                        <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
				                        <button type="submit" class="btn">Sign in</button>
				                    </form>
			                    </div>
		                    </div>          
                        </div>
                        <div class="col-sm-1 middle-border"></div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-5">
                        	<div class="form-box">
                        		<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>Register</h3>
	                            		<p>Register to get access for secure API</p>
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="fa fa-pencil"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
				                    <form role="form" action="<c:url value="/home"/>" method="post" class="registration-form">
				                    	<div class="form-group">
				                    		<label class="sr-only" for="form-first-name">University</label>
				                        	<input type="text" value="${university.name}" name="name" placeholder="University..." class="form-first-name form-control" id="form-first-name">
				                        	<span class="text-danger"><form:errors path="university.name" /></span>
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-last-name">Password</label>
				                        	<input type="password" value="${university.pass}" name="pass" placeholder="Password..." class="form-last-name form-control" id="form-last-name">
				                        </div>
				                        <span class="text-danger"><form:errors path="university.pass" /></span>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-email">Confirm</label>
				                        	<input type="password" name="confirm" placeholder="Confirm..." class="form-email form-control" id="form-email">
				                        </div>
				                        <span class="text-danger"><form:errors path="university.confirm"/></span>
				                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				                        <button type="submit" class="btn">Register</button>
				                    </form>
			                    </div>
                        	</div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-box">
                                <div class="form-top">
                                    <div class="form-top-left">
                                        <h3>Documentation for open API</h3>
                                        <p>Build your application with easy access... This open API can use without any registration & credentials</p>
                                        <p><strong>Note</strong> : this API only have get access for results</p>
                                    </div>
                                    <div class="form-top-right">
                                        <i class="fa fa-file-text"></i>
                                    </div>
                                </div>
                                <div class="col-md-12 bgColor">
                                    <p class="col-sm-6 bg">
                                       <br>Get Results - <br><br>
                                       *&nbsp;Request Method - GET <br>
                                       <br>*&nbsp;Request URL - www.resultpoint.com/api/open/{university name}/{index number} <br>
                                       <br>*&nbsp;Response Type - JSON<br>
                                     </p> 
                                    <p class="col-sm-6 bg">
                                        <br>Response Objects - <br><br>
                                       *&nbsp;Success Response <br>
                                       &nbsp;{<br>
                                       &nbsp;&nbsp;&nbsp;&nbsp;indexNo : "{index number of result}",<br>
                                       &nbsp;&nbsp;&nbsp;&nbsp;type : "{type of result}",<br>
                                       &nbsp;&nbsp;&nbsp;&nbsp;rClass : "{result class}",<br>
                                       &nbsp;&nbsp;&nbsp;&nbsp;marks : "{marks}",<br>
                                       &nbsp;}<br><br>
                                       *&nbsp;Error Response <br>
                                       &nbsp;{<br>
                                       &nbsp;&nbsp;&nbsp;&nbsp;eMsg : "{Error Message}",<br>
                                       &nbsp;&nbsp;&nbsp;&nbsp;statusCode : "{http status code}",<br>
                                       &nbsp;}<br><br>
                                    </p>  
                                </div>
                            </div>          
                        </div>   
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer -->
        <footer>
        	<div class="container">
        		<div class="row">
        			
        			<div class="col-sm-8 col-sm-offset-2">
        				<div class="footer-border"></div>
        				
        			</div>
        			
        		</div>
        	</div>
        </footer>

        <!-- Javascript -->
        <script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/jquery.backstretch.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/scripts.js"/>"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>


<!-- Mirrored from azmind.com/demo/bootstrap-login-register-forms/form-2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Apr 2016 09:28:24 GMT -->
</html>