'use strict';

App.factory('SystemService', ['$http', '$q','UrlService', function($http,$q,UrlService){
	return {
		getPortsByDeviceType: function(typeId){
			return $http.get(UrlService.baseUrl+'system/'+typeId+'/get-ports-by-type')
			.then(
					function(response){
						return response.data;
					}, 
					function(errResponse){
						console.error('Error while fetching customer names');
						return $q.reject(errResponse);
					}
			);
		},
		getDefultMeasuresByVendType: function(vendId){
			return $http.get(UrlService.baseUrl+'system/'+vendId+'/get-default-measures-by-vend-type-id')
			.then(
					function(response){
						return response.data;
					}, 
					function(errResponse){
						console.error('Error while fetching customer names');
						return $q.reject(errResponse);
					}
			);
		},
		getProfileDetails: function(profileId){
			return $http.get(UrlService.baseUrl+'system/'+profileId+'/get-vend-measure-profile-by-id')
			.then(
					function(response){
						return response.data;
					}, 
					function(errResponse){
						console.error('Error while gettin profile data');
						return $q.reject(errResponse);
					}
			);
		}
	};
}]);