'use strict';

App.factory('DashboardService', ['$http', '$q','UrlService', function($http,$q,UrlService){
	return {
		getDTableData: function(typeId){
			return $http.get(UrlService.baseUrl+'system/get-vend-table-data')
			.then(
					function(response){
						return response.data;
					}, 
					function(errResponse){
						console.error('Error while fetching dashboard table data');
						return $q.reject(errResponse);
					}
			);
		}
	};
}]);