'use strict';

Capp.factory('ChartService', ['$http', '$q','UrlService', function($http,$q,UrlService){
	return {
		getChartData: function(vendId){
			return $http.get(UrlService.baseUrl+'system/'+vendId+'/get-chart-data')
			.then(
					function(response){
						return response.data;
					}, 
					function(errResponse){
						console.error('Error while fetching customer names');
						return $q.reject(errResponse);
					}
			);
		},
		getVolChartData: function(typeId){
			return $http.get(UrlService.baseUrl+'system/'+typeId+'/get-ports-by-type')
			.then(
					function(response){
						return response.data;
					}, 
					function(errResponse){
						console.error('Error while fetching customer names');
						return $q.reject(errResponse);
					}
			);
		}
	};
}]);