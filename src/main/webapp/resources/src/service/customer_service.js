'use strict';

App.factory('CustomerService', ['$http','$q','UrlService',function($http,$q,UrlService){
	return {
		getCustomerNameLike : function(name){
			return $http.get(UrlService.baseUrl+'customer/'+name+'/customer-name')
			.then(
					function(response){
						return response.data;
					}, 
					function(errResponse){
						console.error('Error while creating user');
						return $q.reject(errResponse);
					}
			);
		},
		getOutlets : function(name){
			return $http.get(UrlService.baseUrl+'customer/'+name+'/customer-outlets')
			.then(
					function(response){
						return response.data;
					},
					function(errResponse){
						return $q.reject(errResponse);
					}
			);
		}
	};
}]);