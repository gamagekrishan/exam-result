'use strict';

App.controller('DashboardController',['$scope', 'DashboardService','$log','$interval', function($scope, DashboardService,$log,$interval){
	self = this;
	
	//charts values
	self.vendRunningVal = 0;
	self.vendMaintainVal = 0;
	self.vendPowDownVal = 0;
	self.vendSwitchOffVal = 0;
	self.vendUnknownVal = 0;
	self.deviceOnlineVal = 0;
	self.deviceOfflineVal = 0;
	self.deviceNotConfiguredVal = 0;
	
	//charts enable and disable flag
	self.vendRunningFlag = true;
	self.vendMaintainFlag = true;
	self.vendPowDownFlag = true;
	self.vendSwitchOffFlag = true;
	self.vendUnknownFlag = true;
	self.deviceOnlineFlag = true;
	self.deviceOfflineFlag = true;
	self.deviceNotConfiguredFlag = true;
	
	//table data set
	//this is a array of object
	self.mTableData = [];
	
	//sync func working flag
	var sync;
	
	//chart options data
	self.vendMaintainOpt = {};
	self.vendPowDownOpt = {};
	self.vendSwitchOffOpt = {};
	self.vendRunningOpt = {};
	self.vendUnknownOpt = {};
	self.deviceOnlineOpt = {};
	self.deviceOfflineOpt = {};
	
	//initialize the charts option data
	//this func get when page load within ng-init
	self.chartInit = function(){
		var opt = {
			      displayPrevious: false,
			      readOnly: true,
			      min: 0,
			      max: 100,
			      barCap: 0,
			      size:90,
			      trackWidth: 15,
			      barWidth: 15,
			      fontSize:20,
			      textColor:'#00A65A',
			      barColor: '#00A65A',
			      trackColor: '#F7F7F6',
			      dynamicOptions: true
			    };
		self.vendMaintainOpt = opt;
		self.vendPowDownOpt = opt;
		self.vendRunningOpt = opt;
		self.vendUnknownOpt = opt;
		self.deviceOnlineOpt = opt;
		self.deviceOfflineOpt = opt;
		self.deviceNotConfiguredOpt = opt;
		self.vendSwitchOffOpt = opt;
	}
	
	
	//change chart option data according to value of particular chart
	function chartStatusChange(options,val,max,min){
		options.min = min;
		options.max = max;
		var range = max - min;
		var avg = (val/range)*100;
			$log.info("after in one -");
		if(avg > 25 && avg < 75){
			options.barColor = "#5FE663";
			options.textColor = "#5FE663";
		}
		if(avg <= 25){
			options.barColor = "#E6E45F";
			options.textColor = "#E6E45F";
		}
		if(avg >= 75 ){
			options.barColor = "#E6675F";
			options.textColor = "#E6675F";
		}
	}
	
	//sync start func
	//this will start data all dashboard data getting requests
	self.syncStart = function(){
		getAllData();
		if ( angular.isDefined(sync) ) return;
		sync = $interval(function(){
			getAllData();
		},30000);
	};
	
	//get data from remote server by using Dashboard service 
	function getAllData(){
		var res = DashboardService.getDTableData();
		res.then(
				function(data){
					self.mTableData = data.tableData;
					chartDataHandler(data.charData);
				},
				function(error){
					$log.info(error);
				});
	}
	
	//handle all charts data
	//func get call when new data received from remote server 
	function chartDataHandler(data){
		if(self.deviceOnlineFlag){
			self.deviceOnlineVal = data.onlineDeviceCount;
			chartStatusChange(self.deviceOnlineOpt,data.onlineDeviceCount,data.allStation,0);
		}
		if(self.vendRunningFlag){
			self.vendRunningVal = data.runningVendCount;
			chartStatusChange(self.vendRunningOpt,data.runningVendCount,data.allStation,0);
		}
		if(self.vendMaintainFalg){
			self.vendMaintainVal = data.maintainVendCount;
			chartStatusChange(self.vendMaintainOpt,data.maintainVendCount,data.allStation,0);
		}
		if(self.vendSwitchOffFlag){
			self.vendSwitchOffVal = data.switchOffVendCount;
			chartStatusChange(self.vendSwitchOffOpt,data.switchOffVendCount,data.allStation,0);
		}
		if(self.vendUnknownFlag){
			self.vendUnknownVal = data.unknownVendCount;
			chartStatusChange(self.vendUnknownOpt,data.unknownVendCount,data.allStation,0);
		}
		if(self.deviceOfflineFlag){
			self.deviceOfflineVal = data.offlineDeviceCount;
			chartStatusChange(self.deviceOfflineOpt,data.offlineDeviceCount,data.allStation,0); 
		}
		
		
		//these two not developed yet
//		self.deviceNotConfiguredVal = data.
//		chartStatusChange(self.deviceNotConfiguredOpt,data.,data.allStation,0);
//		self.vendPowDownVal = data.PowerDownVendCount;
//		chartStatusChange(self.vendPowDownOpt,data.PowerDownVendCount,data.allStation,0);
	}
	
}]);