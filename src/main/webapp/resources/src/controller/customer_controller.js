'use strict';

App.controller('CustomerController',['$scope', 'CustomerService','$log', function($scope, CustomerService,$log){
	self = this;
	
	self.CustomerSearch = function(query){
		return CustomerService.getCustomerNameLike(query);
	};
	
	self.onSelectPart = function(item){
		$log.info("item - "+item);
	}
	
}]);