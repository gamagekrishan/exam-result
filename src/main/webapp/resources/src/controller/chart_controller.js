'use strict';

Capp.controller('ChartController',['$scope', 'ChartService','$log','$interval', function($scope, ChartService,$log,$interval){
	self = this;
	self.vendId = "";
	self.chartAlert = false;
	self.chartMsg = "";
	self.tempChartAlert = false;
	self.tempChartMsg = "";
	self.tempChartInit = false;
	
	self.volChartAlert = false;
	self.volChartMsg = "";
	self.volChartInit = false;
	
	self.currentChartAlert = false;
	self.currentChartMsg = "";
	self.currentChartInit = false;
	
	var sync;
	
	
	
	Highcharts.setOptions({
        global: {
          useUTC: false
        }
      });
	
	self.add = function(){
		msgShow(true,"new Message");
		var today = new Date();
		var n = {
	    	id: 'Volume',
	    	name: 'Volume 1',
	    	data: [[today,100]],
	    	tooltip: {
                valueSuffix: ' C'
            }
	    };
		$scope.chartConfig.series.push(n);
	}
	
	
	self.syncStart = function(){
		$log.info("station id -"+self.vendId );
		syncData(self.vendId);
		if ( angular.isDefined(sync) ) return;
		sync = $interval(function(){
			syncData(self.vendId);
		},30000);
	}
	
	function msgShow(flag,msg){
		self.chartAlert = flag;
		self.chartMsg = msg;
	}
	
	function msgTempShow(flag,msg){
		self.tempChartAlert = flag;
		if(self.tempChartMsg != ""){
			self.tempChartMsg =  self.tempChartMsg+" </br> "+msg;
		}else{
			self.tempChartMsg = msg;
		}
	}
	
	function msgVolShow(flag,msg){
		self.volChartAlert = flag;
		if(self.volChartMsg != ""){
			self.volChartMsg =  self.volChartMsg+" </br> "+msg;
		}else{
			self.volChartMsg = msg;
		}
	}
	
	function msgCurrentShow(flag,msg){
		self.currentChartAlert = flag;
		if(self.currentChartMsg != ""){
			self.currentChartMsg =  self.currentChartMsg+" </br> "+msg;
		}else{
			self.currentChartMsg = msg;
		}
	}
	
	
	
	function syncData(vendId){
		if(vendId != ""){
			var res = ChartService.getChartData(vendId);
			res.then(
					function(data){
						setChartData(data);
					},
					function(error){
						$log.info(error);
					}
			);
		}
	}
	
	
	function setChartData(data){
		//$log.info(data);
		self.chartAlert = false;
		self.chartMsg = "";
		if(data.deviceStatus == 1){
			if(data.vendStatus == 1){
				setTempChartData(data.tempList,self.tempChartConfig.series);
				setVolChartData(data.volList,self.volChartConfig.series);
				setCurrentChartData(data.currentList,self.currentChartConfig.series);
			}else{
				msgShow(true,"System is offline, so there is no data to show");
			}
		}else{
			msgShow(true,"Station is offline, so there is no data to show");
		}
		
	}
	
	function setVolChartData(dataList,series){
		$log.info(dataList);
		self.volChartAlert = false;
		self.volChartMsg = "";
		if(dataList.length > 0){
			angular.forEach(dataList,function(dValue,dKey){
				//$log.info("val count -"+dValue.valCount);
				//check measure have values
				if(dValue.valCount == 1){
					var sContain = false;
					var sK = 0;
					angular.forEach(series,function(sValue,sKey){
						if(sValue.id == dValue.measure){
							sContain = true;
							sK = sKey;
						}
					});
					if(sContain){
						addDataToSeries(series,sK,dValue.time,dValue.value);
					}else{
						var nSeries = {
						    	id: dValue.measure,
						    	name: dValue.measure,
						    	data: [],
						    	tooltip: {
					                valueSuffix: ' ml'
					            }
						    };
						addSeriesToChart(series,nSeries);
					}
				}else{
					if(dValue.status == 1){
						msgVolShow(true,dValue.measure+" not working properly please check it out");
					}else{
						msgVolShow(true,dValue.measure+" measure set off");
					}
				}
			});
			if(!self.volChartInit){
				setDefault(series);
			}
			self.volChartInit = true;
		}else{
			msgVolShow(true,self.vendId +" do not have Volume measures");
		}
	}
	
	function setCurrentChartData(volList,series){
		
	}
	
	function setTempChartData(dataList,series){
		self.tempChartAlert = false;
		self.tempChartMsg = "";
		if(dataList.length > 0){
			angular.forEach(dataList,function(dValue,dKey){
				//$log.info("val count -"+dValue.valCount);
				if(dValue.valCount == 1){
					var sContain = false;
					var sK = 0;
					angular.forEach(series,function(sValue,sKey){
						if(sValue.id == dValue.measure){
							sContain = true;
							sK = sKey;
						}
					});
					if(sContain){
						addDataToSeries(series,sK,dValue.time,dValue.value);
					}else{
						var nSeries = {
						    	id: dValue.measure,
						    	name: dValue.measure,
						    	data: [],
						    	tooltip: {
					                valueSuffix: ' C'
					            }
						    };
						addSeriesToChart(series,nSeries);
					}
				}else{
					if(dValue.status == 1){
						msgTempShow(true,dValue.measure+" not working properly please check it out");
					}else{
						msgTempShow(true,dValue.measure+" measure set off");
					}
				}
			});
			if(!self.tempChartInit){
				setDefault(series);
			}
			self.tempChartInit = true;
		}else{
			msgTempShow(true,self.vendId +" do not have temperature measures");
		}
	}
	
	
	function addDataToSeries(series,key,x,y){
		series[key].data.push([x, y]);
		if(series[key].data.length > 20){
			series[key].data.splice(0,1);
		}
	}
	
	function addSeriesToChart(series,nSeries){
		series.push(nSeries);
	}
	
	
	function setDefault(series){
	  	angular.forEach(series,function(value,key){
	  		$log.info("value id -"+value.id);
	  		$log.info("key  -"+key);
	  		var date = new Date();
	  		var mil = date.getTime();
	  		var colData = mil - (30000*20);
	  			for (var i=1; i<20; i++) {
	  				var colData = colData + 30000;
	  					series[key].data.push([colData, 0]);
	  			    }
	  	});
	}
	  
	  
	  self.currentChartConfig = {
			  
			    options: {
			    	
			      chart: {
			        type: 'spline',
					zoomType: 'x',
			      },
			      noData: {
			    	  attr:{},
			    	  position: {"x": 0, "y": 0, "align": "center", "verticalAlign": "middle"},
			    	  style:{"fontSize": "12px", "fontWeight": "bold", "color": "#60606a"},
			    	  useHTML:false
			      },
				  navigator: {
					  adaptToUpdatedData: false,
			            enabled: true,
			            series: {
			                data: []
			            }
			      },
			      plotOptions: {
			        series: {
			          stacking: ''
			        }
			      },
			      noData: {
			    	  attr:{},
			    	  position: {"x": 0, "y": 0, "align": "center", "verticalAlign": "middle"},
			    	  style:{"fontSize": "36px", "fontWeight": "bold", "color": "#F36C51"},
			    	  useHTML:false
			      },
				  xAxis: [{
							gridLineWidth: 1,
			                type: 'datetime',
							title: {text: 'Time'}
							
			            }],
				yAxis: [{ // Primary yAxis

		            min: 0,
		            allowDecimals: true,
		            title: {
		                text: 'Voltage (V)',
		                style: {
		                    color: '#80a3ca'
		                }
		            },
		            labels: {
		                format: '{value} V',
		                style: {
		                    color: '#80a3ca'
		                }
		            }


		        },
		        { // Secondary yAxis
		            min: 0,
		            allowDecimals: true,
		            title: {
		                text: 'Current',
		                style: {
		                    color: '#c680ca'
		                }
		            },
		            labels: {
		                format: '{value} A',
		                style: {
		                    color: '#c680ca'
		                }
		            },
		            opposite: true

		        }],
				tooltip: {
						shared: true,
			            crosshairs: true
			        },
			        plotOptions: {
		                series: {
		                    lineWidth: 1,
		                    fillOpacity: 0.5,
		                    marker: {
		                        enabled: true
		                    }

		                }
		            }
			    },
			    series: [{
			    	id: 'voltage',
			    	name: 'Voltage',
			    	data: [],
			    	type: 'spline',
		            yAxis: 0,
		            color: '#80a3ca',
			    	tooltip: {
		                valueSuffix: ' V'
		            }
			    },{
			    	id: 'current',
			    	name: 'Current',
			    	data: [],
			    	type: 'spline',
		            yAxis: 1,
		            tooltip: {
		                valueSuffix: ' A'
		            },
		            color: '#c680ca'
			    },
			    ],
			    title: {
			      text: 'Power values'
			    },
			    credits: {
			      enabled: false
			    },
			    loading: false
			  };
	
	
	self.volChartConfig = {
			  
		    options: {
		    	
		      chart: {
		        type: 'spline',
				zoomType: 'x',
		      },
		      plotOptions: {
		        series: {
		          stacking: ''
		        }
		      },
		      noData: {
		    	  attr:{},
		    	  position: {"x": 0, "y": 0, "align": "center", "verticalAlign": "middle"},
		    	  style:{"fontSize": "36px", "fontWeight": "bold", "color": "#F36C51"},
		    	  useHTML:false
		      },
			  xAxis: [{
						gridLineWidth: 1,
		                type: 'datetime',
						title: {text: 'Time'}
						
		            }],
			yAxis: [{
				allowDecimals: true,
				labels: {
	                format: '{value} ml'
	            },
				title: {text: 'Volume (ml)'}
			}],
			tooltip: {
		            shared: true,
	                crosshairs: true
		        },
			plotOptions: {
		                series: {
		                    lineWidth: 1,
		                    fillOpacity: 0.5,
		                    marker: {
		                        enabled: true
		                    }

		                }
		            }
		    },
		    series: [],
		    title: {
		      text: 'Volume values'
		    },
		    credits: {
		      enabled: false
		    },
		    loading: false
		  };
	  
	self.tempChartConfig = {
			  
		    options: {
		    	
		      chart: {
		        type: 'spline',
				zoomType: 'x',
		      },
		      plotOptions: {
		        series: {
		          stacking: ''
		        }
		      },
		      noData: {
		    	  attr:{},
		    	  position: {"x": 0, "y": 0, "align": "center", "verticalAlign": "middle"},
		    	  style:{"fontSize": "36px", "fontWeight": "bold", "color": "#F36C51"},
		    	  useHTML:false
		      },
			  xAxis: [{
						gridLineWidth: 1,
		                type: 'datetime',
						title: {text: 'Time'}
						
		            }],
			yAxis: [{
				allowDecimals: true,
				labels: {
	                format: '{value} C'
	            },
				title: {text: 'Temperature (C)'}
			}],
			tooltip: {
		            shared: true,
	                crosshairs: true
		        },
			plotOptions: {
		                series: {
		                    lineWidth: 1,
		                    fillOpacity: 0.5,
		                    marker: {
		                        enabled: true
		                    }

		                }

		            }
		    },
		    series: [],
		    title: {
		      text: 'Temperature values'
		    },
		    credits: {
		      enabled: false
		    },
		    loading: false
		  };
	
}]);