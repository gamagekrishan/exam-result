'use strict';

App.controller('SystemController', ['$scope','$log','SystemService','CustomerService', function($scope, $log, SystemService,CustomerService){
	var self = this;
	self.customerWaiting = false;
	self.outletWaiting = false;
	self.deviceTypeWaiting = false;
	self.profile;
	self.vendType;
	self.ports = [];
	$scope.outletList = ["krishan","kaaml"];
	self.vendMeasureList = [{measure: '',status:'1',reference:'',min:'',max:'',normal:'',portId:"",portMsg:false,typeId:'',typeName:'',selectedTypeId:'',selectedTypeName:''}];
    self.measureList = [];
    self.vendType = "";
    self.deviceTypeId = "";
    self.selectProfileWaiting = false;
    
    //max measures count according to device type
    self.maxMeasures = 10;
    
    //max temperature measures of device type
    self.maxTempCount = 10;
    
    //max volume measures count of device type
    self.maxVolCount = 10;
	
	self.addMeasureList = function(measureId,measureName,measureType,id){
		self.measureList.push({measureId:measureId,measureName:measureName,measureType:measureType,measureTypeId:id});
		//$log.info(measureType);
	}
	
	//this function will trigger when port selected from port select drop down
	self.selectPort = function(portId,vendM){
		$log.info("port id of selectPort - "+portId);
		CheckTypeValidity(vendM);
	}
	//use to check selected measure type can measure by selected port
	 function CheckTypeValidity(vendM){
		 $log.info(vendM);
		 $log.info("CheckTypeValidity get call");
		 $log.info("selected port id - "+ vendM.portId);
		 if(vendM.portId != '' && vendM.measure != ''){
			 $log.info("in if");
			var portMeasureTypeId = null;
			var measureTypeId = vendM.typeId;
			$log.info("selected measure type id -" +measureTypeId);
			var port = null;
			for(var key in self.ports){
				if(self.ports[key].id == vendM.portId){
					port = self.ports[key];
				}
			}
			
			$log.info("................ports measure types............");
			for(var typeKey in port.measureTypes){
				$log.info("type -"+port.measureTypes[typeKey].id);
				if(port.measureTypes[typeKey].id == measureTypeId){
					portMeasureTypeId = port.measureTypes[typeKey].id;
				}
			}
			if(portMeasureTypeId!= null){
				vendM.portMsg = false;
			}else{
				vendM.portMsg = true;
			}
			
		 }else{
			 vendM.portMsg = false;
		 }
	}
	 
	 //this function get call when select vend system type
	 self.vendTypeChange = function(vendType){
		 //$log.info("vend type id - "+vendType);
		 removeDefaultMeasures();
		 if(vendType != ''){
			 var res = SystemService.getDefultMeasuresByVendType(vendType);
			 res.then(
					 function(data){
						 addDefaultMeasureToVendMeasureList(data);
					 },
					 function(err){
						 $log.info(err);
					 });
		 }
	 }
	 
	 
	 //remove default measures when change system type
	 function removeDefaultMeasures(){
		for(var i=0;i <self.vendMeasureList.length;i++){
			 if(self.vendMeasureList[i].defaultFlag == true){
				 //$log.info(i);
				 self.removeItem(i);
				 --i;
			 }
		 }
	 }
 
    self.CustomerSearch = function(query){
    	
    	//$log.info('Text changed to ' + query);
    	self.customerWaiting = true;
    	var l = CustomerService.getCustomerNameLike(query);
    	l.then(
    			function(data){
    				self.customerWaiting = false;
    			}
    	);
    	//$log.info(l);
    	
    	return l;
    };
  
    self.portIndex = function(index){
    	if(self.ports.length > 1 && index >= 0){
	    	//$log.info("func get called - "+index);
	    	var rep = ""+self.ports[index].name;
	    	if(self.ports[index].measureTypes.length >= 1){
	    		rep = rep+" ( ";
	    		for(var key in self.ports[index].measureTypes){
	    			if(self.ports[index].measureTypes.length > 1 && key != 0){
	    				rep = rep+" , ";
	    			}
	    			//$log.info(self.ports[index].measureTypes[key].type);
	    			rep = rep + self.ports[index].measureTypes[key].type;
	    		}
	    		rep = rep+" )";
	    	}
	    	return rep;
    	}
    }
    
    //add default measure to vendMeasureList 
    //this function will call when select vend type from drop down 
    function addDefaultMeasureToVendMeasureList(measureList){
    	//$log.info(measureList);
    	for(var key in measureList){
    		var measure = measureList[key];
    		//$log.info(measure);
    		addMeasureToMeasureList(0,measure.id,1,'','','','','',
					false,measure.measureType.id,measure.measureType.type,'',measure.measureType.type,true);
    	}
    }
    
    self.changDeviceType = function(deviceTypeId){
    	if(deviceTypeId > 0){
	    	var res = SystemService.getPortsByDeviceType(deviceTypeId);
	    	res.then(
	    			function(data){
	    				self.ports = data;
	    				setDeviceTypePortThresholds(self.ports);
	    				//$log.info(data);
	    			},
	    			function(error){
	    				$log.info(error);
	    			}
	    	);
    	}else{
    		self.ports = [];
    	}
    }
    
    self.onCustomerSelect = function(customer){
    	var d = CustomerService.getOutlets(customer);
    	d.then(function(data){
    		$scope.outletList = data;
    	},function(err){
    		$log.info(err);
    	});
    }
    
    self.getFunc = function(val,vendM){
    	if(val == ''){
    		vendM.selectedTypeName = '';
    		vendM.portMsg = false;
    	}else{
	    	for(var key in self.measureList){
	    		if(self.measureList[key].measureId == val){
	    			vendM.selectedTypeName = self.measureList[key].measureType;
	    			vendM.typeId = self.measureList[key].measureTypeId;
	    		}
	    	}
	    	CheckTypeValidity(vendM);
    	}
    }
    
    function setDeviceTypePortThresholds(ports){
    	self.maxMeasures = ports.length;
    	self.maxVolCount = 0;
    	self.maxTempCount = 0;
    	for(var key in ports){
    		for(var typeKey in ports[key].measureTypes){
    			if(ports[key].measureTypes[typeKey].type == "temperature"){
    				self.maxTempCount++;
    			}
    			if(ports[key].measureTypes[typeKey].type == "volume"){
    				self.maxVolCount++;
    			}
    		}
    	}
    	
    	
    }

	self.addMeasure = function(index){
		if(self.maxMeasures > self.vendMeasureList.length){
			addMeasureToMeasureList(index,'',1,'','','','','',
					false,'','','','',false);
			
		}
	}
	
	function addMeasureToMeasureList(index,measure,status,reference,min,max,normal,portId,
							portMsg,typeId,typeName,selectedTypeId,selectedTypeName,defaultFlag){
		$log.info("type id - "+typeId);
		$log.info("status id - "+status);
		var measure = {
				measure: ""+measure,
				status:""+status,
				reference:reference,
				min:""+min,
				max:""+max,
				normal:""+normal,
				portId:""+portId,
				portMsg:portMsg,
				typeId:""+typeId,
				typeName:typeName,
				selectedTypeId:""+selectedTypeId,
				selectedTypeName:selectedTypeName,
				defaultFlag:defaultFlag
		};
		self.vendMeasureList.splice(index, 0, measure);
		/*$log.info("-----------------------added measure------------------------");
		$log.info(measure);
		$log.info("-----------------------all measures------------------------");
		$log.info(self.vendMeasureList);*/
	}
	
	self.removeItem = function(index){
		if(self.vendMeasureList.length > 1)
		{
			self.vendMeasureList.splice(index, 1);
		}
	}
	
	self.changeSystemProfile = function(profileId){
		//$log.info("profile id - "+profileId);
		
		if(profileId != ""){
			self.selectProfileWaiting = true;
			var res = SystemService.getProfileDetails(profileId);
			res.then(
					function(data){
						if(data != null){
							self.vendMeasureList = [];
							manageSetProfile(data);
							self.selectProfileWaiting = false;
						}
						
					},
					function(err){
						$log.info(err);
						self.selectProfileWaiting = false;
					});
			
		}
	}
	
	function manageSetProfile(profileData){
		self.vendType = ""+profileData.vendType.id;
		self.deviceTypeId = ""+profileData.deviceType.id;
		self.ports = profileData.deviceType.ports;
		setDeviceTypePortThresholds(self.ports);
		var defaultMeasureIds = [];
		var defaultMeasureList = profileData.vendType.defaultMeasures;
		for(var key in defaultMeasureList){
			defaultMeasureIds.push(defaultMeasureList[key].id);
		}
		var defCount = 0;
		for(var key in profileData.vendMeasureProfileMeasures){
			var m = profileData.vendMeasureProfileMeasures[key];
			$log.info("measure id"+m.measure.id);
			$log.info("measure port id - "+m.port.id);
			var measure = m.measure.id;
			var status = m.status;
			var reference = m.reference;
			var min = m.min;
			var max = m.max;
			var normal = m.normal;
			var portId = m.port.id;
			var portMsg = false;
			var typeId = m.measure.id;
			var typeName;
			var selectedTypeId;
			var selectedTypeName = m.measure.measureType.type ;
			var defaultFlag = false;
			var index = defCount;
			if(isInArray(m.measure.id, defaultMeasureIds)){
				defaultFlag = true;
				defCount++;
				index = 0;
			}
			addMeasureToMeasureList(index,measure,status,reference,min,max,normal,portId,
					portMsg,typeId,typeName,selectedTypeId,selectedTypeName,defaultFlag)
		}
		
	}
	
	function isInArray(value, array) {
		  return array.indexOf(value) > -1;
		}
	

	
}]);



