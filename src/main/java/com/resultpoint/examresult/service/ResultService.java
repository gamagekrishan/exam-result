package com.resultpoint.examresult.service;

import java.util.Collection;

import com.resultpoint.examresult.model.Result;
import com.resultpoint.examresult.model.University;

public interface ResultService {

	public Result getResult(String uName,String indexNo);
	public Result saveResult(Result result);
	public long getResultCountOfUniversity(int id);
	public Collection<Result> getResults(University university);
	public Result getResult(String indexNo);
	public Result updateResult(int id,Result result);
	public Result deleteResult(Result result);
}
