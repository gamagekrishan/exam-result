package com.resultpoint.examresult.service;

public interface TokenGeneratorService {

	public String generateToken();
}
