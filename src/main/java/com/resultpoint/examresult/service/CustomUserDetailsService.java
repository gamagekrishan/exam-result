package com.resultpoint.examresult.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.resultpoint.examresult.dao.UniversityDao;
import com.resultpoint.examresult.model.University;


@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UniversityDao universityDao;
	
	@Transactional(readOnly=true)
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		System.out.println("interface get call - user -"+userName);
		University u = universityDao.getUniversityByName(userName);
		System.out.println("get user name -"+u.getName());
		if(u == null){
			System.out.println("University name not found");
			throw new UsernameNotFoundException("University name not found");
		}
		return new org.springframework.security.core.userdetails.User(u.getName(), u.getHashPass(), 
					 true, true, true, true, getGrantedAuthorities(u));
		
		
	}

	private List<GrantedAuthority> getGrantedAuthorities(University university){
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		/*for(UserProfile userProfile : user.getUserProfiles()){
			System.out.println("UserProfile : "+userProfile);
			authorities.add(new SimpleGrantedAuthority("ROLE_"+userProfile.getType()));
		}*/
		System.out.print("authorities :"+authorities);
		return authorities;
	}
}
