package com.resultpoint.examresult.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.resultpoint.examresult.dao.UniversityDao;
import com.resultpoint.examresult.model.University;


@Service("universityService")
@Transactional
public class UniversityServiceImpl implements UniversityService{

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UniversityDao universityDao;
	
	@Autowired
	private TokenGeneratorService tokengeneratorService;
	
	@Override
	public int saveUniversity(University university) {
		university.setHashPass(passwordEncoder.encode(university.getPass()));
		university.setRegisterDate(new Date());
		return universityDao.saveUniversity(university); 
	}

	@Override
	public University getUniversityByName(String name) {
		return universityDao.getUniversityByName(name);
	}

	@Override
	public void saveToken(University university) {
		university.setToken(tokengeneratorService.generateToken());
		universityDao.updateUniversity(university);
	}

	@Override
	public University getUniversityByToken(String token) {
		return universityDao.getUniversityByToken(token);
	}

}
