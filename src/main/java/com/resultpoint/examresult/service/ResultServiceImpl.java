package com.resultpoint.examresult.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.resultpoint.examresult.dao.ResultDao;
import com.resultpoint.examresult.dao.UniversityDao;
import com.resultpoint.examresult.model.Result;
import com.resultpoint.examresult.model.University;

@Service("resultService")
public class ResultServiceImpl implements ResultService {

	@Autowired
	private ResultDao resultDao;
	
	@Autowired
	private UniversityDao universityDao; 
	
	@Override
	public Result getResult(String uName, String indexNo) {
		Result result = resultDao.getResult(uName, indexNo);
		if(result != null){
			this.incResutlViewCount(uName);
		}
		return result;
	}

	@Override
	public Result saveResult(Result result) {
		return resultDao.saveResult(result);
	}

	@Override
	public long getResultCountOfUniversity(int id) {
		return resultDao.getResultCountOfUniversity(id);
	}

	@Override
	public Collection<Result> getResults(University university) {
		Collection<Result> results =  resultDao.getResults(university);
		if(results.size() > 0)
		{
			this.incResutlViewCount(university.getName());
		}
		return results;
	}

	@Override
	public Result getResult(String indexNo) {
		return resultDao.getResult(indexNo);
	}

	@Override
	public Result updateResult(int id, Result result) {
		result.setId(id);
		return resultDao.updateResult(result);
	}

	@Override
	public Result deleteResult(Result result) {
		return resultDao.deleteResult(result);
	}
	
	private void incResutlViewCount(String uniName){
		University uni = universityDao.getUniversityByName(uniName);
		uni.setViewCount(uni.getViewCount()+1);
		universityDao.updateUniversity(uni);
	}

}
