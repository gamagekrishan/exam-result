package com.resultpoint.examresult.service;

import com.resultpoint.examresult.model.University;

public interface UniversityService {

	public int saveUniversity(University university);
	public University getUniversityByName(String name);
	public void saveToken(University university);
	public University getUniversityByToken(String token);
}
