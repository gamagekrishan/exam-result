package com.resultpoint.examresult.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;


import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="result")
public class Result {

	
	private int id;
	
	@Size(min=4,max=15)
	private String indexNo;
		
	private ResultType type;

	private ResultClass rClass;

	@Max(100)
	private double marks = -1;
	
	private University university;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="index_no", unique = true, nullable = false, length = 15)
	public String getIndexNo() {
		return indexNo;
	}
	
	public void setIndexNo(String indexNo) {
		this.indexNo = indexNo;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="result_type",nullable = false)
	public ResultType getType() {
		return type;
	}
	public void setType(ResultType type) {
		this.type = type;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="result_class", nullable = false)
	public ResultClass getrClass() {
		return rClass;
	}
	public void setrClass(ResultClass rClass) {
		this.rClass = rClass;
	}
	
	@Column(name="marks", precision=10, scale=2)
	public double getMarks() {
		return marks;
	}
	public void setMarks(double marks) {
		this.marks = marks;
	}

	@ManyToOne
	@JoinColumn(name="uni_id")
	@JsonIgnore
	public University getUniversity() {
		return university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}
	
}
