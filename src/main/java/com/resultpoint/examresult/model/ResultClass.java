package com.resultpoint.examresult.model;

public enum ResultClass {

	FIRST_CLASS(1001),
	SECOND_CLASS_UPPER(1002),
	SECOND_CLASS_LOWER(1003),
	THIRD_CLASS(1004);
	
	private int code;
	
	private ResultClass(int code){
		this.code = code;
	}
	
	public Integer getCode(){
		return this.code;
	}
}
