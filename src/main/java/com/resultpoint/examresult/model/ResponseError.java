package com.resultpoint.examresult.model;

public class ResponseError {

	private String eMsg;
	private int statusCode;
	
	public ResponseError(){}
	
	public ResponseError(String eMsg,int statusCode){
		this.eMsg = eMsg;
		this.statusCode = statusCode;
	}
	
	public String geteMsg() {
		return eMsg;
	}
	public void seteMsg(String eMsg) {
		this.eMsg = eMsg;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	
	
}
