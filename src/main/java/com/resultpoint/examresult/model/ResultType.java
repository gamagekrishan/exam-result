package com.resultpoint.examresult.model;

public enum ResultType {

	INFORMATION_TECHNOLOGY(1001),
	MANAGMENT(1002),
	SCIENCE(1005),
	ENGINEERING(1006),
	SOFTWARE_ENGINEERING(1007);
	
	private int code;
	
	private ResultType(int code){
		this.code = code;
	}
	
	public Integer getCode(){
		return this.code;
	}
	
}
