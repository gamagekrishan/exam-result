package com.resultpoint.examresult.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "university")
public class University {

	private int id;
	
	@Size(min=5,max=255)
	private String name;
	
	@JsonIgnore
	private String hashPass;
	
	@JsonIgnore
	@Size(min=5,max=20)
	private String pass;
	
	@JsonIgnore
	private String token;
	
	private Collection<Result> results = new ArrayList<Result>();
	
	private Date registerDate;
	
	private int viewCount = 0;
	
	@JsonIgnore
	private String confirm;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="name", unique = true, nullable = false, length = 255)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="pass", nullable = false, length = 65)
	public String getHashPass() {
		return hashPass;
	}
	public void setHashPass(String pass) {
		this.hashPass = pass;
	}
	
	@Column(name="token", unique = true, nullable = true, length = 65)
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	@OneToMany(mappedBy = "university")
	public Collection<Result> getResults() {
		return results;
	}
	public void setResults(Collection<Result> results) {
		this.results = results;
	}
	
	@Column(name="reg_date")
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	
	@Column(name="view_count")
	public int getViewCount() {
		return viewCount;
	}
	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}
	
	@Transient
	public String getConfirm() {
		return confirm;
	}
	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}
	
	@Transient
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
}
