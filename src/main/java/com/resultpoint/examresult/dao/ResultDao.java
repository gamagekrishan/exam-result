package com.resultpoint.examresult.dao;

import java.util.Collection;

import com.resultpoint.examresult.model.Result;
import com.resultpoint.examresult.model.University;

public interface ResultDao {
	public Result getResult(String uName,String indexNo);
	public Result saveResult(Result result);
	public long getResultCountOfUniversity(int id);
	public Collection<Result> getResults(University university);
	public Result getResult(String indexNo);
	public Result updateResult(Result result);
	public Result deleteResult(Result result);
}
