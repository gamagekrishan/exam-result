package com.resultpoint.examresult.dao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.resultpoint.examresult.model.University;

@Repository("universityDao")
public class UniversityDaoImpl extends Dao implements UniversityDao {

	@Override
	public int saveUniversity(University university) {
		Session session = this.getSessionFactory().openSession();
		Transaction tx = null;
		int id = 0;
		try{
			tx = session.beginTransaction();
			session.save(university);
			id = university.getId();
			session.getTransaction().commit();
		}catch(HibernateException e){
			if(tx != null){
				tx.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
		return id;
	}

	@Override
	public University getUniversityByName(String name) {
		Session session = this.getSessionFactory().openSession();
		University university = null;
		try{
			Criteria criteria = session.createCriteria(University.class)
					.add(Restrictions.eq("name", name));
			university = (University)criteria.uniqueResult();
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return university;
	}

	@Override
	public void updateUniversity(University university) {
		Session session = this.getSessionFactory().openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			session.update(university);
			session.getTransaction().commit();
		}catch(HibernateException e){
			if(tx != null){
				tx.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
	}

	@Override
	public University getUniversityByToken(String token) {
		Session session = this.getSessionFactory().openSession();
		University university = null;
		try{
			Criteria criteria = session.createCriteria(University.class)
					.add(Restrictions.eq("token", token));
			university = (University)criteria.uniqueResult();
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return university;
	}
}
