package com.resultpoint.examresult.dao;

import com.resultpoint.examresult.model.University;

public interface UniversityDao {

	public int saveUniversity(University university);
	public University getUniversityByName(String name);
	public void updateUniversity(University university);
	public University getUniversityByToken(String token);
	
}
