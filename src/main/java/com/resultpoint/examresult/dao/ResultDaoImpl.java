package com.resultpoint.examresult.dao;


import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.resultpoint.examresult.model.Result;
import com.resultpoint.examresult.model.University;


@Repository("resultDao")
public class ResultDaoImpl extends Dao implements ResultDao {

	@Override
	public Result getResult(String uName, String indexNo) {

		Session session = this.getSessionFactory().openSession();
		Result result = null;
		try{
			Criteria criteria = session.createCriteria(Result.class)
					.createAlias("university", "u")
					.add(Restrictions.eq("u.name", uName))
					.add(Restrictions.eq("indexNo", indexNo));
			
			result = (Result)criteria.uniqueResult();
		}catch (HibernateException e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return result;
	}

	@Override
	public Result saveResult(Result result) {
		Session session = this.getSessionFactory().openSession();
		Transaction tx = null;
		Result sResult = null;
		try{
			tx = session.beginTransaction();
			session.save(result);
			sResult = result;
			session.getTransaction().commit();
		}catch(HibernateException e){
			if(tx != null){
				tx.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
		return sResult;
	}

	@Override
	public long getResultCountOfUniversity(int id) {
		Session session = this.getSessionFactory().openSession();
		long count = 0;
		try{
			Criteria criteria = session.createCriteria(Result.class)
					.createAlias("university", "uni")
					.add(Restrictions.eq("uni.id", id))
					.setProjection(Projections.rowCount());
			count = (long)criteria.uniqueResult();
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Result> getResults(University university) {
		Session session = this.getSessionFactory().openSession();
		Collection<Result> results = null;
		try{
			Criteria criteria = session.createCriteria(Result.class)
					.add(Restrictions.eq("university", university));
			results = (Collection<Result>)criteria.list();
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return results;
	}

	@Override
	public Result getResult(String indexNo) {
		Session session = this.getSessionFactory().openSession();
		Result result = null;
		try{
			Criteria criteria = session.createCriteria(Result.class)
					.add(Restrictions.eq("indexNo", indexNo));
			result = (Result)criteria.uniqueResult();
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return result;
	}

	@Override
	public Result updateResult(Result result) {
		Session session = this.getSessionFactory().openSession();
		Transaction tx = null;
		Result uResult = null;
		try{
			tx = session.beginTransaction();
			session.update(result);
			session.getTransaction().commit();
			uResult = result;
		}catch(HibernateException e){
			if(tx != null){
				tx.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
		return uResult;
	}

	@Override
	public Result deleteResult(Result result) {
		Session session = this.getSessionFactory().openSession();
		Transaction tx = null;
		Result dResult = null;
		try{
			tx = session.beginTransaction();
			session.delete(result);
			session.getTransaction().commit();
			dResult = result;
		}catch(HibernateException e){
			if(tx != null){
				tx.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
		return dResult;
	}
}
