package com.resultpoint.examresult.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.resultpoint.examresult.model.ResponseError;
import com.resultpoint.examresult.model.Result;
import com.resultpoint.examresult.model.University;
import com.resultpoint.examresult.service.ResultService;
import com.resultpoint.examresult.service.UniversityService;

@RestController
@RequestMapping("/api/secure")
public class SecureApiController {

	@Autowired
	private UniversityService universityService;
	
	@Autowired
	private ResultService resultService;
	
	//GET - get all result of particular university
	@RequestMapping(value="/{token}", produces = MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.GET)
	public ResponseEntity<Object> getResults(@PathVariable("token") String token){
		University uni = universityService.getUniversityByToken(token);
		if(uni != null){
			Collection<Result> results = resultService.getResults(uni);
			if(!results.isEmpty()){
				return new ResponseEntity<Object>(results,HttpStatus.OK);
			}
			return new ResponseEntity<Object>(new ResponseError("Results not found",HttpStatus.NOT_FOUND.value()),HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>(new ResponseError("Authentication fail",HttpStatus.UNAUTHORIZED.value()),HttpStatus.UNAUTHORIZED);
	}
	
	//POST - save result by JSON object
	@RequestMapping(value="/{token}", produces = MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.POST)
	public ResponseEntity<Object> saveResults(@Valid @RequestBody Result result, BindingResult bindErrors ,@PathVariable("token") String token){
		System.out.println("post get call");
		University uni = universityService.getUniversityByToken(token);
		if(uni != null){
			if(bindErrors.hasErrors()){
				return new ResponseEntity<Object>(bindErrors.getFieldError().getDefaultMessage(),HttpStatus.CONFLICT);
			}
			result.setUniversity(uni);
			Result sResult = resultService.saveResult(result);
			if(sResult != null){
				return new ResponseEntity<Object>(sResult,HttpStatus.OK);
			}
			return new ResponseEntity<Object>(result,HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<Object>(new ResponseError("Authentication fail",HttpStatus.UNAUTHORIZED.value()),HttpStatus.UNAUTHORIZED);
	}
	
	//GET - get a result by index number
	@RequestMapping(value="/{indexNo}/{token}", produces = MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.GET)
	public ResponseEntity<Object> getResult(@PathVariable("indexNo") String indexNo, @PathVariable("token") String token){
		University uni = universityService.getUniversityByToken(token);
		if(uni != null){
			Result result = resultService.getResult(indexNo);
			if(result != null){
				return new ResponseEntity<Object>(result,HttpStatus.OK);
			}
			return new ResponseEntity<Object>(new ResponseError("Result not found",HttpStatus.NOT_FOUND.value()),HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>(new ResponseError("Authentication fail",HttpStatus.UNAUTHORIZED.value()),HttpStatus.UNAUTHORIZED);
	}
	
	//PUT - update result by JSON object
	@RequestMapping(value="/{indexNo}/{token}", produces = MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.PUT)
	public ResponseEntity<Object> updateResult(@Valid @RequestBody Result rResult, BindingResult bindErrors , @PathVariable("token") String token,@PathVariable("indexNo") String indexNo){
		University uni = universityService.getUniversityByToken(token);
		if(uni != null){
			if(bindErrors.hasErrors()){
				return new ResponseEntity<Object>(bindErrors.getFieldError().getDefaultMessage(),HttpStatus.CONFLICT);
			}
			Result result = resultService.getResult(indexNo);
			if(result != null){
				rResult.setUniversity(uni);
				Result uResult = resultService.updateResult(result.getId(), rResult);
				if(uResult != null){
					return new ResponseEntity<Object>(uResult,HttpStatus.OK);
				}
				return new ResponseEntity<Object>(rResult,HttpStatus.EXPECTATION_FAILED);	
			}
			return new ResponseEntity<Object>(rResult,HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>(new ResponseError("Authentication fail",HttpStatus.UNAUTHORIZED.value()),HttpStatus.UNAUTHORIZED);
	}
	
	//DELETE - delete result by id
	@RequestMapping(value="/{indexNo}/{token}", produces = MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.DELETE)
	public ResponseEntity<Object> deleteResult(@PathVariable("token") String token , @PathVariable("indexNo") String indexNo){
		University uni = universityService.getUniversityByToken(token);
		if(uni != null){
			Result result = resultService.getResult(indexNo);
			if(result != null){
				Result dResult = resultService.deleteResult(result);
				if(dResult != null){
					return new ResponseEntity<Object>(result,HttpStatus.OK);
				}
				return new ResponseEntity<Object>(result,HttpStatus.EXPECTATION_FAILED);
			}
			return new ResponseEntity<Object>(new ResponseError("Result not found",HttpStatus.NOT_FOUND.value()),HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>(new ResponseError("Authentication fail",HttpStatus.UNAUTHORIZED.value()),HttpStatus.UNAUTHORIZED);
	}
	
}
