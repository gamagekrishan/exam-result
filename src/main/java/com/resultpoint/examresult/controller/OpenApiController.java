package com.resultpoint.examresult.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.resultpoint.examresult.model.ResponseError;
import com.resultpoint.examresult.model.Result;
import com.resultpoint.examresult.service.ResultService;

@RestController
@RequestMapping("/api/open")
public class OpenApiController {

	@Autowired
	private ResultService resultService;
	
	@RequestMapping(value="/{uName}/{indexNo}", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getResult(@PathVariable("uName") String uniName, @PathVariable("indexNo") String indexNo){
		
		Result result = resultService.getResult(uniName, indexNo);
		if(result != null){
			return new ResponseEntity<Object>(result,HttpStatus.OK);
		}else{
			return new ResponseEntity<Object>(new ResponseError("Result not found",HttpStatus.NOT_FOUND.value()),HttpStatus.NOT_FOUND);
		}
	}
}
