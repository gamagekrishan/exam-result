package com.resultpoint.examresult.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.resultpoint.examresult.model.Result;
import com.resultpoint.examresult.model.ResultClass;
import com.resultpoint.examresult.model.ResultType;
import com.resultpoint.examresult.model.University;
import com.resultpoint.examresult.service.ResultService;
import com.resultpoint.examresult.service.SecurityService;
import com.resultpoint.examresult.service.TokenGeneratorService;
import com.resultpoint.examresult.service.TokenGeneratorServiceImpl;
import com.resultpoint.examresult.service.UniversityService;

@Controller
public class WebController {

	@Autowired 
	SecurityService securityService;
	
	@Autowired
	UniversityService universityService;
	
	@Autowired
	ResultService resultService;
	
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		
		if (error != null) {
			model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("home");
		return model;
	}
	
	// customize the error message
	private String getErrorMessage(HttpServletRequest request, String key) {

		Exception exception = (Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "Invalid university name or password!";
		} else if (exception instanceof LockedException) {
			error = exception.getMessage();
		} else {
			error = "Invalid university name or password!";
		}
		return error;
	}
	
	@RequestMapping("/show-user")
	public ModelAndView user(){
		ModelAndView model = new ModelAndView();
		RestTemplate t = new RestTemplate(); 
		String url = "http://localhost:8080/resultpoint/api/user";
		University u = null;
		try{
			u = t.getForObject(url, University.class);
			System.out.println("user name -"+u.getName());
		}catch(HttpStatusCodeException e){
			int statusCode = e.getStatusCode().value();
			String res = e.getResponseBodyAsString();
			System.out.println("User obj -"+u);
			System.out.println("response obj as string -"+res);
			System.out.println("status code -"+statusCode);
		} catch (RestClientException ex) {
		    // Some other error occured, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		return model;
	}
	
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String showDashboardPage(ModelMap model) {
        model.addAttribute("user", securityService.findLoggedInUsername());
        System.out.println("loged user name -"+securityService.findLoggedInUsername());
        University uni = universityService.getUniversityByName(securityService.findLoggedInUsername());
        model.addAttribute("uni", uni);
        model.addAttribute("count", resultService.getResultCountOfUniversity(uni.getId()));
        return "dashboard";
    }
	
	@RequestMapping(value = "/dashboard", method = RequestMethod.POST)
    public String generateToken(ModelMap model) {
        University uni = universityService.getUniversityByName(securityService.findLoggedInUsername());
        universityService.saveToken(uni);
        return "redirect:/dashboard";
    }
    
    @RequestMapping(value = "/home", method = RequestMethod.POST)
    public ModelAndView register(@Valid @ModelAttribute("university") University university, 
    		BindingResult result) {
    	
    	ModelAndView model = new ModelAndView();
    	
    	if(!university.getPass().equals(university.getConfirm())){
    		result.rejectValue("confirm", "error.university", "password is not match");
    	}
    	if(result.hasErrors()){
    		model.setViewName("home");
    		return model;
    	}
    	universityService.saveUniversity(university);
    	securityService.autologin(university.getName(), university.getPass());
    	model.setViewName("redirect:/dashboard");
    	return model;
    }
    
    @RequestMapping("/test")
	public String save(){
		//System.out.println("encode pass -"+passwordEncoder.encode("11111"));
		return "";
	}
	
//	System.out.println("reg get call");
//	securityService.autologin("krishan", "11111");
//
//    return "redirect:/secure";
}
