package com.apiconsumer.apiconsumer.model;

public class Result {

	private String indexNo;
		
	private ResultType type;

	private ResultClass rClass;

	private double marks;


	public ResultType getType() {
		return type;
	}

	public void setType(ResultType type) {
		this.type = type;
	}

	public ResultClass getrClass() {
		return rClass;
	}

	public void setrClass(ResultClass rClass) {
		this.rClass = rClass;
	}

	public double getMarks() {
		return marks;
	}

	public void setMarks(double marks) {
		this.marks = marks;
	}

	public String getIndexNo() {
		return indexNo;
	}

	public void setIndexNo(String indexNo){
		this.indexNo = indexNo;
	}
	
	
}
