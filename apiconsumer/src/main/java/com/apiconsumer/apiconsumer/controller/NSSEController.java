package com.apiconsumer.apiconsumer.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.apiconsumer.apiconsumer.model.Result;

@Controller
@RequestMapping("/nsse")
public class NSSEController {

	@Value("${TOKEN}")
	private String token;
	
	@RequestMapping("/view")
	public ModelAndView index(){
		ModelAndView model = new ModelAndView("view-result");
		RestTemplate t = new RestTemplate(); 
		String url = "http://localhost:8080/resultpoint/api/secure/"+token;
		List<Result> results = null;
		String resErr = "";
		try{
			ResponseEntity<List<Result>> rateResponse =
			        t.exchange(url,HttpMethod.GET, null, new ParameterizedTypeReference<List<Result>>(){});
			results = rateResponse.getBody();
			System.out.println("result size -"+results.get(0).getIndexNo());
		}catch(HttpStatusCodeException e){
			if(e.getStatusCode() == HttpStatus.NOT_FOUND){
				resErr = "Results Not found";
				System.out.println();
			}else{
				resErr = "error occured while getting results";
			}
		} catch (RestClientException ex) {
			ex.printStackTrace();
		    // Some other error occured, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		model.addObject("err", resErr);
		model.addObject("results", results);
		return model;
	}
	
	@RequestMapping(value="/add", method = RequestMethod.GET)
	public ModelAndView showAddPage(){
		ModelAndView model = new ModelAndView("add-result");
		model.addObject("err", "");
		return model;
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(@ModelAttribute("result") Result result){
		RestTemplate t = new RestTemplate(); 
		String url = "http://localhost:8080/resultpoint/api/secure/"+token;
		String resErr = "";
		Result sResult = null;
		try{
		    RestTemplate restTemplate = new RestTemplate();
		    restTemplate.postForObject(url, result, Result.class);
		}catch(HttpStatusCodeException e){
			if(e.getStatusCode() == HttpStatus.EXPECTATION_FAILED){
				resErr = "'"+result.getIndexNo()+"' couldn't save due to invalid data";
			}else{
				resErr = "error occured while saving result";
			}
		} catch (RestClientException ex) {
			ex.printStackTrace();
		    // Some other error occured, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		return "redirect:/nsse/view";
	}
	
	@RequestMapping(value = "/delete/{indexNo}", method = RequestMethod.GET)
	public String delete(@PathVariable("indexNo") String indexNo){
		RestTemplate t = new RestTemplate(); 
		String url = "http://localhost:8080/resultpoint/api/secure/"+indexNo+"/"+token;
		Result dResult = null;
		String resErr = "";
		
		try{
			ResponseEntity<Result> rateResponse =
			        t.exchange(url,HttpMethod.DELETE, null, new ParameterizedTypeReference<Result>(){});
			dResult = rateResponse.getBody();
			System.out.println("user name -"+dResult.getIndexNo());
		}catch(HttpStatusCodeException e){
			if(e.getStatusCode() == HttpStatus.NOT_FOUND){
				resErr = "'"+indexNo+"' not exist";
			}else{
				resErr = "error occured while deleting result";
			}
			
		} catch (RestClientException ex) {
			ex.printStackTrace();
		    // Some other error occured, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		return "redirect:/nsse/view";
	}
	
	@RequestMapping(value = "/edit/{indexNo}", method = RequestMethod.POST)
	public ModelAndView edit(@ModelAttribute("result") Result result, @PathVariable("indexNo") String indexNo){
		ModelAndView model = new ModelAndView();
		RestTemplate t = new RestTemplate(); 
		String url = "http://localhost:8080/resultpoint/api/secure/{indexNo}/"+token;
		String resErr = "";
		
		try{
			Map<String, String> params = new HashMap<String, String>();
		    params.put("indexNo", indexNo);
		    t.put ( url, result, params);
		    model.setViewName("redirect:/nsse/view");
		}catch(HttpStatusCodeException e){
			model.setViewName("add-result");
			model.addObject("err", resErr);
			if(e.getStatusCode() == HttpStatus.NOT_FOUND){
				resErr = "'"+indexNo+"' not exist";
			}else{
				resErr = "error occured while updating result";
			}
			
		} catch (RestClientException ex) {
			ex.printStackTrace();
		    // Some other error occured, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		return model;
	}
	
	@RequestMapping(value = "/edit/{indexNo}" , method = RequestMethod.GET)
	public ModelAndView editShowPage(@PathVariable("indexNo") String indexNo){
		ModelAndView model = new ModelAndView("edit-result");
		String url = "http://localhost:8080/resultpoint/api/secure/"+indexNo+"/"+token;
		Result result = null;
		String err = "";
		RestTemplate restTemplate = new RestTemplate();
		try{
			result = restTemplate.getForObject(url, Result.class);
			System.out.println("result index -"+result.getIndexNo());
		}catch(HttpStatusCodeException e){
			if(e.getStatusCode() == HttpStatus.NOT_FOUND){
				err = "'"+indexNo+"' not exist";
			}else{
				err = "error occured while updating result";
			}
		} catch (RestClientException ex) {
			ex.printStackTrace();
		    // Some other error occurred, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		model.addObject("result", result);
		model.addObject("err", err);
		return model;
	}
	
	@RequestMapping(value = "/search" , method = RequestMethod.POST)
	public ModelAndView search(@RequestParam("index") String indexNo){
		ModelAndView model = new ModelAndView("view-result");
		String url = "http://localhost:8080/resultpoint/api/secure/"+indexNo+"/"+token;
		List<Result> results = new ArrayList<Result>();
		String err = "";
		RestTemplate restTemplate = new RestTemplate();
		try{
			Result result = restTemplate.getForObject(url, Result.class);
			results.add(result);
			System.out.println("result index -"+result.getIndexNo());
		}catch(HttpStatusCodeException e){
			if(e.getStatusCode() == HttpStatus.NOT_FOUND){
				err = "'"+indexNo+"' not exist";
			}else{
				err = "error occured while search result";
			}
		} catch (RestClientException ex) {
			ex.printStackTrace();
		    // Some other error occurred, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		model.addObject("results", results);
		model.addObject("err", err);
		return model;
	}
}
