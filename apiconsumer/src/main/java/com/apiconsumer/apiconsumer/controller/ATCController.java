package com.apiconsumer.apiconsumer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/atc")
public class ATCController {

	@RequestMapping("/application")
	public String index(){
		System.out.println("app get call");
		return "application-submit-form";
	}
}
