<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Appliation Submit Form</title>

	<link rel="stylesheet" href="assets/demo.css">
	<link rel="stylesheet" href="assets/form-mini.css">
    <link rel="stylesheet" href="assets/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<header>
		<h1><strong>ACT (pvt)Ltd.</strong></h1>
    </header>
<body  style="background-color:#f2f2f2">
   


    <div class="main-content">

        <!-- You only need this form and the form-mini.css -->

        <div class="form-mini-container">

            <h1 style="padding-top:10px;">Basic Information</h1>

            <form class="form-mini" method="post" action="#" style="margin-left:-130px; width:600px">

                <div class="form-row">
                    <b style="font-size:120%">First Name:</b>
                    <input type="text" name="lastName" placeholder="Enter last name here" style="width:390px; margin-left:55px;">
                    <span style="width:390px; margin-left:55px;">Krishan Dhanushka</span><i class="fa fa-check" style="color:green"></i>
                </div>

                <div class="form-row">
                    <b style="font-size:120%">Last Name:</b>
                    <input type="text" name="lastName" placeholder="Enter last name here" style="width:390px; margin-left:55px;">
                </div>
                
                <div class="form-row">
                    <b style="font-size:120%">Date Of Birthday:</b>
                    <input type="text" name="dob" placeholder="Enter your date of birth" style="width:390px; margin-left:10px;">
                </div>
                
                <div class="form-row">
                    <b style="font-size:120%">Gender:</b>
                    <label style="margin-left:10px;">
                        <select name="dropdowngender" style="margin-left:74px;">
                            <option>Choose an option</option>
                            <option>Male</option>
                            <option>Female</option>
                        </select>
                    </label>
                </div>
                
                <div class="form-row">
                    <b style="font-size:120%">Address:</b>
                    <input type="text" name="address" placeholder="Enter your address" style="width:390px; margin-left:75px;">
                </div>
                
                <div class="form-row">
                    <b style="font-size:120%">City:</b>
                    <input type="text" name="city" placeholder="Enter city name" style="width:390px; margin-left:110px;">
                </div>
                
                <div class="form-row">
                    <b style="font-size:120%">First Name:</b>
                    <input type="text" name="state" placeholder="Enter your state" style="width:390px; margin-left:55px;">
                </div>

            </form>
        </div>
        
        <div class="form-mini-container">
            <h1 style="padding-top:10px; margin-top:10px;">Education Qualification</h1>
            <form class="form-mini" method="post" action="#" style="margin-left:-130px; width:600px;">
            
             <div class="form-row">
                    <b style="font-size:120%">Univercity (Institute):</b>
                    <input type="text" name="uni" placeholder="Enter your state" style="width:370px; margin-left:18px;">
             </div>
             
             <div class="form-row">
                    <b style="font-size:120%">Registration No:</b>
                    <input type="text" name="regno" placeholder="Enter your state" style="width:370px; margin-left:55px;">
             </div> 
             
             <div class="form-row form-last-row">
                    <button type="submit">Add Record</button>
                    <button type="submit">Clrear</button>
                    <button type="submit">Cancle</button>
             </div>
                
            </form>
       </div>

    </div>

</body>

</html>
