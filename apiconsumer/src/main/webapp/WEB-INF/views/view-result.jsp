<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Home</title>
	<link href=" <c:url value="/resources/css/demo.css"/> " rel="stylesheet" />
	<link href=" <c:url value="/resources/css/form-basic.css"/> " rel="stylesheet" />
	<link href=" <c:url value="/resources/css/form-search.css"/> " rel="stylesheet" />
    <link href=" <c:url value="/resources/css/bootstrap.css"/> " rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

</head>
    <header>
		<h1>Client Application</h1>
    </header>
    
 <body  style="background-color:#f2f2f2">
    <ul>
        <li><a href="<c:url value="/nsse/view"/>" class="active">Home</a></li>
        <li><a href="<c:url value="/nsse/add"/>">Add New</a></li>
    </ul>


    <div class="main-content">

        <form class="form-search" method="post" action="<c:url value="/nsse/search"/>">
            <input type="text" name="index" placeholder="Inndex No...">
            <button type="submit">Search</button>
            <i class="fa fa-search"></i>
        </form>
        <c:if test="${err != ''}">
        	<div class="alert alert-danger" style="text-align:center;" role="alert">${err}</div>    
        </c:if>
        <form class="form-basic" method="post" action="#" style="margin-top:20px;">
            
            <table class="table">
    <thead>
        <tr>
            <th style="text-align: center;">Index No</th>
            <th style="text-align: center;">Type</th>
            <th style="text-align: center;">Class</th>
            <th style="text-align: center;">Marks</th>
            <th style="text-align: center;">Action</th>
        </tr>
    </thead>
    <tbody>
	    <c:forEach items="${results}" var="result">
	    	<tr class="info">
	            <td>${result.indexNo}</td>
	            <td>${result.type}</td>
	            <td>${result.rClass}</td>
	            <td>${result.marks}</td>
	            <td><a href="<c:url value="/nsse/delete/${result.indexNo}"/>"><i class="fa fa-trash"></i></a> | <a href="<c:url value="/nsse/edit/${result.indexNo}"/>">Edit</a></td>
        	</tr>
	    </c:forEach>
    </tbody>
</table>

        </form>

    </div>

</body>

</html>
