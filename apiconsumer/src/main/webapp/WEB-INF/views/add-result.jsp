<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Add New</title>

	<link href=" <c:url value="/resources/css/demo.css"/> " rel="stylesheet" />
	<link href=" <c:url value="/resources/css/form-mini.css"/> " rel="stylesheet" />
    <link href=" <c:url value="/resources/css/bootstrap.css"/> " rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<header>
		<h1>Client Application</h1>
    </header>
<body  style="background-color:#f2f2f2">
    <ul style="height:90px">
        <li style="margin-left:10px;"><a href="<c:url value="/nsse/view"/>">Home</a></li>
        <li><a href="<c:url value="/nsse/add"/>" class="active">Add New</a></li>
    </ul>


    <div class="main-content">

        <!-- You only need this form and the form-mini.css -->

        <div class="form-mini-container">

			<c:if test="${err != '' }">
				<div class="alert alert-danger" role="alert">${err }</div>
			</c:if>
			
            <h1>Add Result</h1>

            <form class="form-mini" method="post" action="#" style="width:600px; margin-left:-125px;">

                <div class="form-row">
                    <b style="font-size:120%">Index No:</b>
                    <input type="text" name="indexNo" placeholder="ID Number" style="width:390px; margin-left:60px;">  
                </div>

                <div class="form-row">
                    <b style="font-size:120%">Degree Type:</b>
                    <label>
                        <select name="type" style="margin-left:32px;width:390px;">
                            <option value="INFORMATION_TECHNOLOGY">INFORMATION TECHNOLOGY</option>
                            <option value="MANAGMENT">MANAGMENT</option>
                            <option value="SCIENCE">SCIENCE</option>
                            <option value="ENGINEERING">ENGINEERING</option>
                            <option value="SOFTWARE_ENGINEERING">SOFTWARE ENGINEERING</option>
                        </select>
                    </label>
                </div>
                
                <div class="form-row">
                    <b style="font-size:120%">Class:</b>
                    <label>
                        <select name="rClass" style="margin-left:86px;width:390px;">
                            <option value="FIRST_CLASS">FIRST CLASS</option>
                            <option value="SECOND_CLASS_UPPER">SECOND CLASS UPPER</option>
                            <option value="SECOND_CLASS_LOWER">SECOND CLASS LOWER</option>
                            <option value="THIRD_CLASS">THIRD CLASS</option>
                        </select>
                    </label>
                </div>
                
                <div class="form-row">
                    <b style="font-size:120%">Marks:</b>
                    <input type="text" name="marks" placeholder="Enter Marks" style="width:390px; margin-left:80px;">  
                </div>
				
                <div class="form-row form-last-row">
                    <button type="submit">Add Result</button>
                    <button onclick="window.location='http://www.example.com';" >Cancel</button>
                </div>

            </form>
        </div>


    </div>

</body>

</html>
