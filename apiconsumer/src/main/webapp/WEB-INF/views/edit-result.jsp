<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Add New</title>

	<link href=" <c:url value="/resources/css/demo.css"/> " rel="stylesheet" />
	<link href=" <c:url value="/resources/css/form-mini.css"/> " rel="stylesheet" />
    <link href=" <c:url value="/resources/css/bootstrap.css"/> " rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<header>
		<h1>Client Application</h1>
    </header>
<body  style="background-color:#f2f2f2">
    <ul style="height:90px">
        <li style="margin-left:10px;"><a href="<c:url value="/nsse/view"/>">Home</a></li>
        <li><a href="form-mini.html" class="active">Add New</a></li>
    </ul>


    <div class="main-content">

        <!-- You only need this form and the form-mini.css -->

        <div class="form-mini-container">


            <h1>Add Result</h1>

            <form class="form-mini" method="post" action="<c:url value="/nsse/edit/${result.indexNo}"/>" style="width:600px; margin-left:-125px;">

                <div class="form-row">
                    <b style="font-size:120%">Index No:</b>
                    <input type="text" name="indexNo" value="${result.indexNo }" placeholder="ID Number" style="width:390px; margin-left:47px;">  
                </div>

                <div class="form-row">
                    <b style="font-size:120%">Degree Type:</b>
                    <label>
                        <select name="type" style="margin-left:32px;width:390px;">
                            <option value="INFORMATION_TECHNOLOGY" <c:if test="${result.type.toString() == 'INFORMATION_TECHNOLOGY'}">selected</c:if>>MATION TECHNOLOGY</option>
                            <option value="MANAGMENT" <c:if test="${result.type.toString() == 'MANAGMENT'}">selected</c:if>>MANAGMENT</option>
                            <option value="SCIENCE" <c:if test="${result.type.toString() == 'SCIENCE'}">selected</c:if>>SCIENCE</option>
                            <option value="ENGINEERING" <c:if test="${result.type.toString() == 'ENGINEERING'}">selected</c:if>>ENGINEERING</option>
                            <option value="SOFTWARE_ENGINEERING" <c:if test="${result.type.toString() == 'SOFTWARE_ENGINEERING'}">selected</c:if>>SOFTWARE ENGINEERING</option>
                        </select>
                    </label>
                </div>
                
                <div class="form-row">
                    <b style="font-size:120%">Class:</b>
                    <label>
                        <select name="rClass" style="margin-left:86px;">
                            <option value="FIRST_CLASS"<c:if test="${result.type.toString() == 'FIRST_CLASS'}">selected</c:if>>FIRST CLASS</option>
                            <option value="SECOND_CLASS_UPPER"<c:if test="${result.type.toString() == 'SECOND_CLASS_UPPER'}">selected</c:if>>SECOND CLASS UPPER</option>
                            <option value="SECOND_CLASS_LOWER"<c:if test="${result.type.toString() == 'SECOND_CLASS_LOWER'}">selected</c:if>>SECOND CLASS LOWER</option>
                            <option value="THIRD_CLASS"<c:if test="${result.type.toString() == 'THIRD_CLASS'}">selected</c:if>>THIRD CLASS</option>
                        </select>
                    </label>
                </div>
                
                <div class="form-row">
                    <b style="font-size:120%">Marks:</b>
                    <input type="text" name="marks" value="${result.marks}" placeholder="Enter Marks" style="width:390px; margin-left:80px;">  
                </div>

                <div class="form-row form-last-row">
                    <button type="submit" class="btn btn-default">Update</button>
                    <a href="<c:url value="/nsse/view"/>" class="btn btn-default">Cancel</a>
                </div>

            </form>
        </div>
    </div>

</body>

</html>
