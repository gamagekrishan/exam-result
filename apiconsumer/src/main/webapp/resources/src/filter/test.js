myapp.controller('tempctrl', function ($scope,$http,$interval) {

	var sync;
	Highcharts.setOptions({
        global: {
          useUTC: false
        }
      });
	  
	//console.log(new Date().getTime());
	$scope.id = "-1";
	$scope.addPoints = function(){
		addPoints($scope);
	};
	$scope.syncStart = function(){
		setDefault($scope);
		syncData($scope,$http,11421);
		if ( angular.isDefined(sync) ) return;
		sync = $interval(function(){
			syncData($scope,$http,11421);
		},30000);
	}


	  $scope.chartConfig = {
			  
	    options: {
	    	
	      chart: {
	        type: 'spline',
			zoomType: 'x',
	      },
		  navigator: {
			  adaptToUpdatedData: false,
	            enabled: true,
	            series: {
	                data: []
	            }
	      },
	      plotOptions: {
	        series: {
	          stacking: ''
	        }
	      },
		  xAxis: [{
					gridLineWidth: 1,
	                type: 'datetime',
					title: {text: 'Time'}
					
	            }],
		yAxis: [{
			allowDecimals: true,
			labels: {
                format: '{value} �C'
            },
			title: {text: 'Temperature (�C)'}
		}],
		tooltip: {
	            shared: true,
                crosshairs: true
	        },
		plotOptions: {
	                series: {
	                    lineWidth: 1,
	                    fillOpacity: 0.5

	                },
	                column: {
	                    stacking: 'normal'
	                },
	                area: {
	                    stacking: 'normal',
	                    marker: {
	                        enabled: false
	                    }
	                }

	            }
	    },
	    series: [{
	    	id: 'temp1',
	    	name: 'Temperature 1',
	    	data: [],
	    	tooltip: {
                valueSuffix: ' �C'
            }
	    },{
	    	id: 'temp2',
	    	name: 'Temperature 2',
	    	data: [],
	    	tooltip: {
                valueSuffix: ' �C'
            }
	    },{
	    	id: 'temp3',
	    	name: 'Temperature 3',
	    	data: [],
	    	tooltip: {
                valueSuffix: ' �C'
            }
	    },{
	    	id: 'temp4',
	    	name: 'Temperature 4',
	    	data: [],
	    	tooltip: {
                valueSuffix: ' �C'
            }
	    }
	    ],
	    title: {
	      text: 'Temperature values'
	    },
	    credits: {
	      enabled: false
	    },
	    loading: false,
	    size: {
	    	height: 500,
	    	width: 1270
	    }
	  }


	});






//get temp data from server
function syncData($scope,$http,deviceId)
{
	  console.log("sync get call");
	 $http.get('http://localhost:8080/Telemtry/system/getcdata/'+deviceId)
	  .then(
			  
	          function(response){
	        	 console.log(response.data);
	        	 addPoints($scope,response.data);
	          }, 
	          function(errResponse){
	          }
	  );
}



//this will add points to temp chart
function addPoints($scope,data) {
	var temp = data.temp;
	console.log("received id-"+temp.id[0]+" scop id - "+$scope.id);
	if(temp.id[0]+"" != $scope.id)
		{
		console.log("id condition true");
		
			$scope.id = temp.id[0]+"";
			console.log("new scope id-"+$scope.id);
			console.log("temp data -------");
			console.log(temp);
		    var seriesArray = $scope.chartConfig.series;
			angular.forEach($scope.chartConfig.series,function(value,key)
			{
				var date = new Date().getTime();
				value.data.splice(0,1);
				console.log(key);
				if(value.id == 'temp1')
				{
					console.log(value.id);
					console.log(new Date(temp.temp1[0]));
					console.log(temp.temp1[0]);
					console.log(date);
					$scope.chartConfig.series[key].data = value.data.concat([[parseInt(temp.temp1[0]),parseFloat(temp.temp1[1])]]);
				}
				if(value.id == 'temp2')
				{
					console.log(value.id);
					$scope.chartConfig.series[key].data = value.data.concat([[parseInt(temp.temp2[0]),parseFloat(temp.temp2[1])]]);
				}
				if(value.id == 'temp3')
				{
					console.log(value.id);
					$scope.chartConfig.series[key].data = value.data.concat([[parseInt(temp.temp3[0]),parseFloat(temp.temp3[1])]]);
				}
				if(value.id == 'temp4')
				{
					console.log(value.id);
					$scope.chartConfig.series[key].data = value.data.concat([[parseInt(temp.temp4[0]),parseFloat(temp.temp4[1])]]);
				}
			});
		}
  };

//set default initial values to chart of temperature

  function setDefault($scope){
  	angular.forEach($scope.chartConfig.series,function(value,key){
  		var date = new Date();
  		//console.log(date);
  		//console.log(date.getTime());
  		var mil = date.getTime();
  		//console.log(mil);
  		var colData = mil - (30000*20);
  		//console.log("get date from mil");
  		//console.log(new Date(colData));
  			for (var i=1; i<20; i++) {
  				var colData = colData + 30000;
  				//console.log("date after add 30000");
  				//console.log(new Date(colData));
  					$scope.chartConfig.series[key].data = value.data.concat([[colData, 0]]);
  			    }
  	});
  }

