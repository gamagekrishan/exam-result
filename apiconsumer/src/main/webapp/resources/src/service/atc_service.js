'use strict';

App.factory('ATCService', ['$http','$q','UrlService',function($http,$q,UrlService){
	return {
		getResult : function(uName, indexNo){
			return $http.get(UrlService.baseUrl+'api/open/'+uName+'/'+indexNo)
			.then(
					function(response){
						return response.data;
					}, 
					function(errResponse){
						console.error('Error while gettin result');
						return $q.reject(errResponse);
					}
			);
		}
	};
}]);