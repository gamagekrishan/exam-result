'use strict';

App.controller('ATCController',['$scope', 'ATCService','$log', function($scope, ATCService,$log){
	self = this;
	
	self.fName;
	self.lName;
	self.age;
	self.gender ="";
	self.uName = "National School of Business Managemant";
	self.dType = "SOFTWARE_ENGINEERING";
	self.indexNo;
	self.alertShow = false;
	self.alertMsg = "";
	self.fSubmit = false;
	self.fSubmits = false;
	self.indexNoV = false;
	self.indexNoI = false;
	self.submitBtn = true;
	self.homeBtn = false;
	
	
	self.CustomerSearch = function(query){
		return CustomerService.getCustomerNameLike(query);
	};
	
	self.submit = function(){
		$log.info("run get call with uName - "+self.uName+" indexNo - "+self.indexNo);
		var res = ATCService.getResult(self.uName,self.indexNo);
		res.then(
				function(data){
					$log.info(data);
					resProc(data);
				},
				function(error){
					$log.info(error);
					errResProc(error.data);
				}
		);
	}
	
	function resProc(data){
		self.fSubmits = true;
		self.indexNoI = false;
		if(data.rClass != "FIRST_CLASS"){
			self.alertMsg = "Your degree class level is not enough to apply for this job";
			self.alertShow = true;
			self.indexNoI = false;
			self.indexNoV = true;
		}else{
			self.fSubmit = true;
			self.indexNoV = true;
			self.submitBtn = false;
			self.homeBtn = true;
			self.alertShow = false;
		}
	}
	
	function errResProc(error){
		self.fSubmits = true;
		if(error.statusCode == 404){
			self.alertMsg = "Invalid Index No";
			self.indexNoI = true;
		}else{
			self.alertMsg = "Some error occured while submiting application, Please try again";
		}
		self.alertShow = true;
	}
	
	
}]);